Webcam.set({
    width: 520,
    height: 440,
    image_format: "jpeg",
    jpeg_quality: 90,
});
// Inititialize frame buffer.

const frameBuffer = new FrameBuffer({ size: 40 });
// Attempt to stream webcam feed to canvas element.
Webcam.attach("#my_camera");
// When webcam feed acquired, executes callback.
Webcam.on('live', startStreamLoop);

var looperPromise;
function startStreamLoop() {
    var TARGET_FPS = 10;
    var looper = function () {
        // Pass current frame image data to handler.
        Webcam.snap(frameCallback);
        looperPromise = setTimeout(looper, 1000 / TARGET_FPS);
    }
    looper();
}

function frameCallback(imgData) {
    // imgData is base64-encoded string of current frame
    // e.g. "data:image(jpeg|png);base64,----"; this is generated in WebcamJS library by calling 
    // canvas.toDataURL('image/jpeg')
    frameBuffer.addFrame(imgData);
    if (frameBuffer.getSize() >= frameBuffer.bufferSize) {
        // Clear buffer, and post frames to data endpoint.
        var data = frameBuffer.getData();
        frameBuffer.clear();
        // DATA_ENDPOINT is API endpoint that handles conversion of image frame sequence to streamable MKV fragment.
        postFrameData(data, DATA_ENDPOINT);
    }
}

function postFrameData(data, endpoint, callback) {
    var $http = new XMLHttpRequest();
    $http.open("POST", endpoint);
    $http.setRequestHeader("Content-Type", "application/json");
    $http.send(JSON.stringify(data));
}

function stopStreaming() {
    try {
        Webcam.reset();
        Webcam.off('live');
        clearTimeout(looperPromise);
        frameBuffer.clear();
    } catch (e) {
        console.error(e);
    }
}
